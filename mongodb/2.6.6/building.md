##compiled on a m3.2xlarge##


- mkdir /tmp/mongodb-linux-x86_64-2.6.6-cordata-with-ssl
- mkdir src; cd src
- sudo apt-get update
- sudo apt-get install build-essential libboost-filesystem-dev libboost-program-options-dev libboost-system-dev libboost-thread-dev scons libssl-dev git
- git clone https://github.com/mongodb/mongo.git
- cd mongo
- git checkout r2.6.6
- scons install -j9 --64 --ssl --prefix=/tmp/mongodb-linux-x86_64-2.6.6-cordata-with-ssl
- cd /tmp
- tar czvf mongodb-linux-x86_64-2.6.6-cordata-with-ssl.tgz mongodb-linux-x86_64-2.6.6-cordata-with-ssl
- exit
- scp -i ~/Downloads/andy-navigator-dev.pem ubuntu@hostname:/tmp/mongodb-linux-x86_64-2.6.6-cordata-with-ssl.tgz .
